import aws_cdk as cdk
import aws_cdk as core
from aws_cdk import (
    Stack,
    aws_s3,

)
import json
import os
from constructs import Construct

class S3DeployStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        cdk_env = cdk.Environment(account=os.getenv('CDK_DEFAULT_ACCOUNT'), region=os.getenv('CDK_DEFAULT_REGION'))

        bucket = aws_s3.Bucket(
            self,
            'coder_bucket',
            bucket_name = 'cdk-bucket-084912487152-us-east-1'
        )

app = core.App()
stack = S3DeployStack(app, "S3DeployStack")
app.synth()


        
