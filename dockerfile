FROM python

COPY . .

RUN python -m pip install aws-cdk-lib

RUN apt update -y 

RUN apt-get install -y nodejs && apt-get install -y npm 

RUN wget nodejs.org/dist/v18.0.0/node-v18.0.0-linux-x64.tar.gz && tar -C /usr/local --strip-components 1 -xzf node-v18.0.0-linux-x64.tar.gz

RUN  npm install -g aws-cdk

