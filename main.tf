# terraform init
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  alias  = "default"
  region = "us-east-1"
}


# For importing resource   
# terraform import 'aws_s3_bucket.bucket' cdk-bucket-084912487152-us-east-1
# After import we can see that our resource in terraform.tfstate
# So if we do "terraform show" it gives us `
resource "aws_s3_bucket" "bucket" {
  bucket                      = "cdk-bucket-084912487152-us-east-1"
  object_lock_enabled         = false
  request_payer               = "BucketOwner"
  tags                        = {}
  tags_all                    = {}

  grant {
    id = "9e81a5d2251b9fc696677dcb0e2f59c7544bf649b1dc2ffc543a0c16948f6b93"
    permissions = [
      "FULL_CONTROL",
    ]
    type = "CanonicalUser"
  }

  server_side_encryption_configuration {
    rule {
      bucket_key_enabled = false

      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled    = false
    mfa_delete = false
  }
}
